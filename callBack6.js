const boards = require("./boards");
const lists = require("./lists");
const cards = require("./cards");
const callBack1 = require("./callBack1");
const callBack2 =require("./callBack2");
const callBack3 = require("./callBack3");


function callBack6(id){
    setTimeout(y=()=>{
         let result = callBack1(id)
         let result1 = callBack2(result().id)
         let result2 = callBack3(result1()[0].id)
         let result3 = callBack3(result1()[1].id)
         let result4 = callBack3(result1()[2].id)
         let result5 = callBack3(result1()[3].id)
         let result6 = callBack3(result1()[4].id)
         return {result2, result3, result4, result5, result6}
    }, 2000)
    return y
}

module.exports = callBack6;
