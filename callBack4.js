const boards = require("./boards");
const lists = require("./lists");
const cards = require("./cards");
const callBack1 = require("./callBack1");
const callBack2 =require("./callBack2");
const callBack3 = require("./callBack3");


function callBack4(id){
    setTimeout(y=()=>{
         let result = callBack1(id)
         let result1 = callBack2(result().id)
         let result2 = callBack3(result1()[0].id)
         return result2();
    }, 2000)
    return y
}

module.exports = callBack4;